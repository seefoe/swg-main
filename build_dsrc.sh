#!/bin/bash
basedir=$PWD
PATH=$basedir/build/bin:$PATH

# build dsrc
cd $basedir
python $basedir/utils/build_dsrc.py
python $basedir/utils/build_object_template_crc_string_tables.py
python $basedir/utils/build_quest_crc_string_tables.py
$basedir/utils/build_miff.sh

echo "Inorder to update crc string tables and quest crc string tables in the database, please run build_linux.sh"


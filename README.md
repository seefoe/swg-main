# SWG Server
Star Wars: Galaxies MMORPG Server

## Instructions
 1. Install Debian 9 x64-bit (I used `debian-9.4.0-amd64-netinst.iso`)
 2. Run `sudo apt-get update`
 3. Run `sudo apt-get install git`
 4. Run `git clone https://bitbucket.org/seefoe/swg-main.git; cd swg-main`
 5. Run `./build.sh`, reboot when it tells you to
 6. `cd` into your `swg-main` folder
 7. Run `./build.sh` again, following the instructions until it completes
 8. Environment should be completely setup and built at this point

## Extra
* If you copy `config.sh.example` to `config.sh`, `build.sh` will load the settings it asks you for from it, so that you don't have to input them every time
* Client content tree files can be built via `utils/build_data_tre.py` and `tools/TreeFileBuilder`, use as follows:
	* `python ./utils/build_data_tre.py <new version> --from <previous version>` 
	* `./tools/TreeFileBuilder -r <patch file list> <output tre name>` 
	* **Examples**
	* `python ./utils/build_data_tre.py 1`, to build the file list for version 1
	* `./tools/TreeFileBuilder -r patch_1.rsp swgs_patch_1.tre`, to build the tree file for version 1
	* `python ./utils/build_data_tre.py 2 --from 1`, to build the file list for version 2 from version 1
	* `./tools/TreeFileBuilder -r patch_2.rsp swgs_patch_2.tre`, to build the tree file for version 2

## Database Setup
Oracle DB can be installed with the Atlas universal oracle preinstaller script, more information and 
instructions can be found [here](https://www.dizwell.com/wordpress/atlas-a-universal-oracle-preinstaller/).
```
wget https://bit.do/dizatlas -O atlas.sh
chmod +x atlas.sh
./atlas.sh 
```
#!/bin/bash

basedir=$PWD
PATH=$PATH:$basedir/build/bin
GIT_URL="https://bitbucket.org/seefoe/"
GIT_REPO_SRC=${GIT_URL}src.git
GIT_REPO_DSRC=${GIT_URL}dsrc.git
GIT_REPO_CONFIG=${GIT_URL}configs.git
GIT_REPO_DATA=${GIT_URL}data.git
GIT_REPO_CLIENTDATA=${GIT_URL}clientdata.git
GIT_REPO_SITE=${GIT_URL}swg-site.git

# load config for build script
if [ -e "config.sh" ]; then
	source config.sh
	echo "Loaded cfg variables from config.sh"
fi

# Debug and Release are for testing and not public servers, they lack optimizations
MODE=Release
#MODE=Debug

if [ ! -d $basedir/build ]
then
	mkdir $basedir/build
fi

if [ ! -f $basedir/.setup ]; then
	if [[ $(lsb_release -a) =~ .*Ubuntu.* ]] || [ -f "/etc/debian_version" ]
	then
		read -p "!!!ONLY RUN ONCE!!! Do you want to install dependencies (y/n)?" response
		response=${response,,} # tolower
		if [[ $response =~ ^(yes|y| ) ]]; then
			$basedir/utils/init/debian.sh
			source /etc/profile.d/java.sh
			source /etc/profile.d/oracle.sh
			touch $basedir/.setup

			echo "Please login and out or reboot as changes have been made to your PATH "
		fi
	fi
fi

read -p "Do you want to pull/update git? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	# update main repo
	git pull

	# SRC
	git clone $GIT_REPO_SRC src
	cd $basedir/src
	git pull
	cd $basedir

	# DSRC
	git clone $GIT_REPO_DSRC dsrc
	cd $basedir/dsrc
	git pull
	cd $basedir

	# CONFIGS
	git clone $GIT_REPO_CONFIG configs
	cd $basedir/configs
	git pull
	cd $basedir

	# CLIENTDATA
	git clone $GIT_REPO_CLIENTDATA data/sku.0/sys.client/compiled/game
	cd $basedir/data/sku.0/sys.client/compiled/game
	git pull
	cd $basedir

	# SITE
	git clone $GIT_REPO_SITE site
	cd $basedir/site
	git pull
	cd $basedir
fi

read -p "Do you want to recompile the server code (C++) now? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	rm -rf $basedir/build
	mkdir $basedir/build
	cd $basedir/build


	if [ $(arch) == "x86_64" ]; then
		export LDFLAGS=-L/usr/lib32
		export CMAKE_PREFIX_PATH="/usr/lib32:/lib32:/usr/lib/i386-linux-gnu:/usr/include/i386-linux-gnu"

		cmake -DCMAKE_C_FLAGS=-m32 \
		-DCMAKE_CXX_FLAGS=-m32 \
		-DCMAKE_EXE_LINKER_FLAGS=-m32 \
		-DCMAKE_MODULE_LINKER_FLAGS=-m32 \
		-DCMAKE_SHARED_LINKER_FLAGS=-m32 \
		-DCMAKE_BUILD_TYPE=$MODE \
		$basedir/src
	else
		cmake $basedir/src -DCMAKE_BUILD_TYPE=$MODE
	fi

	make -j$(nproc)

	cd $basedir
fi

read -p "Do you want to build stationapi (chat backend)? (y/n) " response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]]; then
	cd $basedir/stationapi
	./build.sh
	cd $basedir
fi

read -p "Do you want to build the config environment now? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then

	if [[ -z "$HOSTIP" ]]; then
		echo "Enter your IP address (LAN for port forwarding or internal, outside IP for DMZ)"
		read HOSTIP
	fi

	if [[ -z "$DBSERVICE" ]]; then
		echo "Enter the DSN for the database connection "
		read DBSERVICE
	fi

	if [[ -z "$DBUSERNAME" ]]; then
		echo "Enter the database username "
		read DBUSERNAME
	fi

	if [[ -z "$DBPASSWORD" ]]; then
		echo "Enter the database password "
		read DBPASSWORD
	fi

	if [[ -z "$CLUSTERNAME" ]]; then
		echo "Enter a name for the galaxy cluster "
		read CLUSTERNAME
	fi

	if [ -d $basedir/exe ]; then
		rm -rf $basedir/exe
	fi

	mkdir -p $basedir/exe/linux/logs
	ln -s $basedir/build/bin $basedir/exe/linux/bin
	cp -u $basedir/configs/* $basedir/exe/linux

	for filename in $(find $basedir/exe -name '*.cfg'); do
		sed -i -e "s@DBSERVICE@$DBSERVICE@g" -e "s@DBUSERNAME@$DBUSERNAME@g" -e "s@DBPASSWORD@$DBPASSWORD@g" -e "s@CLUSTERNAME@$CLUSTERNAME@g" -e "s@HOSTIP@$HOSTIP@g" $filename
	done

	#
	# Generate other config files if their template exists.
	#

	# Generate at least 1 node that is the /etc/hosts IP.
	$basedir/utils/build_node_list.sh
fi

if [ ! -d $basedir/data ]; then
	read -p "Symlink to data directory (y/n)? " remote_dsrc_response
	remote_dsrc_response=${remote_dsrc_response,,}
	if [[ $remote_dsrc_response =~ ^(yes|y| ) ]]; then
		read -p "Enter target data directory " DATA_DIR
		ln -s $DATA_DIR ./data
	fi
fi

buildTemplates=false

read -p "Do you want to build dsrc? (y/n) " response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]]; then
	#prepare environment to run data file builders
	oldPATH=$PATH
	PATH=$basedir/build/bin:$PATH
	$basedir/build_dsrc.sh
	buildTemplates=true
fi

if [[ $buildTemplates = false ]]; then
	read -p "Do you want to build the Object Template or Quest CRC files? (y/n) " response
	response=${response,,}
	if [[ $response =~ ^(yes|y| ) ]]; then
		buildTemplates=true
	fi
fi

templatesLoaded=false

if [[ $buildTemplates = true ]]; then
	echo "Object Template and Quest CRC files will now be built and re-imported into the database."

	if [[ -z "$DBSERVICE" ]]; then
		echo "Enter the DSN for the database connection "
		read DBSERVICE
	fi

	if [[ -z "$DBUSERNAME" ]]; then
		echo "Enter the database username "
		read DBUSERNAME
	fi

	if [[ -z "$DBPASSWORD" ]]; then
		echo "Enter the database password "
		read DBPASSWORD
	fi

	#prepare environment to run data file builders
	oldPATH=$PATH
	PATH=$basedir/build/bin:$PATH

	$basedir/utils/build_object_template_crc_string_tables.py
	$basedir/utils/build_quest_crc_string_tables.py

	cd $basedir/src/game/server/database

	echo "Loading template list"

	perl ./templates/processTemplateList.pl < $basedir/dsrc/sku.0/sys.server/built/game/misc/object_template_crc_string_table.tab > $basedir/build/templates.sql
	sqlplus ${DBUSERNAME}/${DBPASSWORD}@${DBSERVICE} @$basedir/build/templates.sql > $basedir/build/templates.out

	templatesLoaded=true

	cd $basedir
	PATH=$oldPATH
fi

read -p "Import database? (y/n) " response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]]; then
	cd $basedir/src/game/server/database/build/linux

	if [[ -z "$DBSERVICE" ]]; then
		echo "Enter the DSN for the database connection "
		read DBSERVICE
	fi

	if [[ -z "$DBUSERNAME" ]]; then
		echo "Enter the database username "
		read DBUSERNAME
	fi

	if [[ -z "$DBPASSWORD" ]]; then
		echo "Enter the database password "
		read DBPASSWORD
	fi

	./database_update.pl --username=$DBUSERNAME --password=$DBPASSWORD --service=$DBSERVICE --goldusername=$DBUSERNAME --loginusername=$DBUSERNAME --createnewcluster --packages

	if [[ $templatesLoaded = false ]]; then
		echo "Loading template list"
		perl ../../templates/processTemplateList.pl < ../../../../../../dsrc/sku.0/sys.server/built/game/misc/object_template_crc_string_table.tab > $basedir/build/templates.sql
		sqlplus ${DBUSERNAME}/${DBPASSWORD}@${DBSERVICE} @$basedir/build/templates.sql > $basedir/build/templates.out
	fi
fi

echo "Build complete!"

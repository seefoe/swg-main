# flush
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT

# default policy to reject
iptables -A INPUT -i lo -j ACCEPT -m comment --comment "Allow all loopback traffic"
iptables -A INPUT ! -i lo -d 127.0.0.0/8 -j REJECT -m comment --comment "Drop all traffic to 127 that doesn't use lo"
iptables -A OUTPUT -j ACCEPT -m comment --comment "Accept all outgoing"
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT -m comment --comment "Allow all incoming on established connections"
iptables -A INPUT -j REJECT -m comment --comment "Reject all incoming"
iptables -A FORWARD -j REJECT -m comment --comment "Reject all forwarded"

# open ports for swg and auth services
iptables -A INPUT -p udp -s X.X.X.X -j ACCEPT
iptables -I INPUT -p tcp --dport 22 -j ACCEPT -m comment --comment "Allow SSH"  
iptables -I INPUT -p udp --dport 44453 -j ACCEPT -m limit --limit 20/s -m comment --comment "Allow SWG Login Server"
iptables -I INPUT -p udp --dport 44462 -j ACCEPT -m limit --limit 10/s -m comment --comment "Allow SWG Ping Server"
iptables -I INPUT -p udp --dport 44463 -j ACCEPT -m limit --limit 75/s -m comment --comment "Allow SWG Connection Server"
iptables -I INPUT -p udp -s X.X.X.X -j ACCEPT

# block attackers
# sample: iptables -I INPUT -s replacemewithip -j DROP -m comment --comment "comment here describing reason to block"
#iptables -I INPUT -s X.X.X.X -j DROP -m comment --comment "01-08-2018 attacker, example example"
